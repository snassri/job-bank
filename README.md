
     ,-----.,--.                  ,--. ,---.   ,--.,------.  ,------.
    '  .--./|  | ,---. ,--.,--. ,-|  || o   \  |  ||  .-.  \ |  .---'
    |  |    |  || .-. ||  ||  |' .-. |`..'  |  |  ||  |  \  :|  `--, 
    '  '--'\|  |' '-' ''  ''  '\ `-' | .'  /   |  ||  '--'  /|  `---.
     `-----'`--' `---'  `----'  `---'  `--'    `--'`-------' `------'
    ----------------------------------------------------------------- 


Welcome to your Django project on Cloud9 IDE!

Your Django project is already fully setup. Just click the "Run" button to start
the application. On first run you will be asked to create an admin user. You can
<<<<<<< HEAD
access your application from 'https://jobbank-snassri-1.c9.io/' and the admin page from 
'https://jobbank-snassri-1.c9.io/admin'.
=======
access your application from 'https://testing-snassri-1.c9.io/' and the admin page from 
'https://testing-snassri-1.c9.io/admin'.
>>>>>>> 0cf092819e48e4e0ac9cc24fc299aef85b1c0165

## Starting from the Terminal

In case you want to run your Django application from the terminal just run:

1) Run syncdb command to sync models to database and create Django's default superuser and auth system

    $ python manage.py syncdb

2) Run Django

    $ python manage.py runserver $IP:$PORT
    
## Support & Documentation

Django docs can be found at https://www.djangoproject.com/

You may also want to follow the Django tutorial to create your first application:
https://docs.djangoproject.com/en/1.7/intro/tutorial01/

Visit http://docs.c9.io for support, or to learn more about using Cloud9 IDE.
To watch some training videos, visit http://www.youtube.com/user/c9ide


## Job Bank Notes

If setting up Job Bank project on a new server, we need to install:
- django uuidfield
- django encrypted_fields
- python dateutil
- djangocms-admin-style (this one by just adding the folder djangocms_admin_style and making sure it's under Installed Apps)