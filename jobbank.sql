-- MySQL dump 10.13  Distrib 5.5.40, for debian-linux-gnu (x86_64)
--
-- Host: 0.0.0.0    Database: jb
-- ------------------------------------------------------
-- Server version	5.5.40-0ubuntu0.14.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `auth_group`
--

DROP TABLE IF EXISTS `auth_group`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(80) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group`
--

LOCK TABLES `auth_group` WRITE;
/*!40000 ALTER TABLE `auth_group` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_group_permissions`
--

DROP TABLE IF EXISTS `auth_group_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_group_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `group_id` (`group_id`,`permission_id`),
  KEY `auth_group_permissions_0e939a4f` (`group_id`),
  KEY `auth_group_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_group_permission_group_id_689710a9a73b7457_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_group__permission_id_1f49ccbbdc69d2fc_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_group_permissions`
--

LOCK TABLES `auth_group_permissions` WRITE;
/*!40000 ALTER TABLE `auth_group_permissions` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_group_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_permission`
--

DROP TABLE IF EXISTS `auth_permission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_permission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(50) NOT NULL,
  `content_type_id` int(11) NOT NULL,
  `codename` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `content_type_id` (`content_type_id`,`codename`),
  KEY `auth_permission_417f1b1c` (`content_type_id`),
  CONSTRAINT `auth__content_type_id_508cf46651277a81_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=34 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_permission`
--

LOCK TABLES `auth_permission` WRITE;
/*!40000 ALTER TABLE `auth_permission` DISABLE KEYS */;
INSERT INTO `auth_permission` VALUES (1,'Can add permission',1,'add_permission'),(2,'Can change permission',1,'change_permission'),(3,'Can delete permission',1,'delete_permission'),(4,'Can add group',2,'add_group'),(5,'Can change group',2,'change_group'),(6,'Can delete group',2,'delete_group'),(7,'Can add user',3,'add_user'),(8,'Can change user',3,'change_user'),(9,'Can delete user',3,'delete_user'),(10,'Can add content type',4,'add_contenttype'),(11,'Can change content type',4,'change_contenttype'),(12,'Can delete content type',4,'delete_contenttype'),(13,'Can add session',5,'add_session'),(14,'Can change session',5,'change_session'),(15,'Can delete session',5,'delete_session'),(16,'Can add log entry',6,'add_logentry'),(17,'Can change log entry',6,'change_logentry'),(18,'Can delete log entry',6,'delete_logentry'),(22,'Can add contact',8,'add_contact'),(23,'Can change contact',8,'change_contact'),(24,'Can delete contact',8,'delete_contact'),(25,'Can add payment',9,'add_payment'),(26,'Can change payment',9,'change_payment'),(27,'Can delete payment',9,'delete_payment'),(28,'Can add submission',10,'add_submission'),(29,'Can change submission',10,'change_submission'),(30,'Can delete submission',10,'delete_submission'),(31,'Can add email',11,'add_email'),(32,'Can change email',11,'change_email'),(33,'Can delete email',11,'delete_email');
/*!40000 ALTER TABLE `auth_permission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user`
--

DROP TABLE IF EXISTS `auth_user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `password` varchar(128) NOT NULL,
  `last_login` datetime NOT NULL,
  `is_superuser` tinyint(1) NOT NULL,
  `username` varchar(30) NOT NULL,
  `first_name` varchar(30) NOT NULL,
  `last_name` varchar(30) NOT NULL,
  `email` varchar(75) NOT NULL,
  `is_staff` tinyint(1) NOT NULL,
  `is_active` tinyint(1) NOT NULL,
  `date_joined` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user`
--

LOCK TABLES `auth_user` WRITE;
/*!40000 ALTER TABLE `auth_user` DISABLE KEYS */;
INSERT INTO `auth_user` VALUES (1,'pbkdf2_sha256$15000$2FhiCDRB8NVK$gsK7XPZ3NKm0ukUEuGNHvEw6aNkaGU0KURczwcxFcA4=','2015-04-16 13:09:26',1,'superadmin','Salma','Nassri','snassri@sogc.com',1,1,'2015-03-19 14:51:38'),(2,'pbkdf2_sha256$15000$eWvaCOq6wZK8$y5fuR4m5/M7Tt2jtnTTKhcq1zoKasw7XfJ9JdREhysM=','2015-04-14 16:49:14',0,'admin','Linda','Kollesh','lkollesh@sogc.com',1,1,'2015-03-31 13:13:39');
/*!40000 ALTER TABLE `auth_user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_groups`
--

DROP TABLE IF EXISTS `auth_user_groups`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_groups` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `group_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`group_id`),
  KEY `auth_user_groups_e8701ad4` (`user_id`),
  KEY `auth_user_groups_0e939a4f` (`group_id`),
  CONSTRAINT `auth_user_groups_group_id_33ac548dcf5f8e37_fk_auth_group_id` FOREIGN KEY (`group_id`) REFERENCES `auth_group` (`id`),
  CONSTRAINT `auth_user_groups_user_id_4b5ed4ffdb8fd9b0_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_groups`
--

LOCK TABLES `auth_user_groups` WRITE;
/*!40000 ALTER TABLE `auth_user_groups` DISABLE KEYS */;
/*!40000 ALTER TABLE `auth_user_groups` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `auth_user_user_permissions`
--

DROP TABLE IF EXISTS `auth_user_user_permissions`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `auth_user_user_permissions` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `user_id` int(11) NOT NULL,
  `permission_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_id` (`user_id`,`permission_id`),
  KEY `auth_user_user_permissions_e8701ad4` (`user_id`),
  KEY `auth_user_user_permissions_8373b171` (`permission_id`),
  CONSTRAINT `auth_user_user_permissi_user_id_7f0938558328534a_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `auth_user_u_permission_id_384b62483d7071f0_fk_auth_permission_id` FOREIGN KEY (`permission_id`) REFERENCES `auth_permission` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `auth_user_user_permissions`
--

LOCK TABLES `auth_user_user_permissions` WRITE;
/*!40000 ALTER TABLE `auth_user_user_permissions` DISABLE KEYS */;
INSERT INTO `auth_user_user_permissions` VALUES (3,2,10),(4,2,11),(5,2,12),(6,2,16),(7,2,17),(8,2,18),(9,2,22),(10,2,23),(11,2,24),(12,2,25),(13,2,26),(14,2,27),(15,2,28),(16,2,29),(17,2,30),(18,2,31),(1,2,32),(2,2,33);
/*!40000 ALTER TABLE `auth_user_user_permissions` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_admin_log`
--

DROP TABLE IF EXISTS `django_admin_log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_admin_log` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_time` datetime NOT NULL,
  `object_id` longtext,
  `object_repr` varchar(200) NOT NULL,
  `action_flag` smallint(5) unsigned NOT NULL,
  `change_message` longtext NOT NULL,
  `content_type_id` int(11) DEFAULT NULL,
  `user_id` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  KEY `django_admin_log_417f1b1c` (`content_type_id`),
  KEY `django_admin_log_e8701ad4` (`user_id`),
  CONSTRAINT `django_admin_log_user_id_52fdd58701c5f563_fk_auth_user_id` FOREIGN KEY (`user_id`) REFERENCES `auth_user` (`id`),
  CONSTRAINT `djang_content_type_id_697914295151027a_fk_django_content_type_id` FOREIGN KEY (`content_type_id`) REFERENCES `django_content_type` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_admin_log`
--

LOCK TABLES `django_admin_log` WRITE;
/*!40000 ALTER TABLE `django_admin_log` DISABLE KEYS */;
INSERT INTO `django_admin_log` VALUES (1,'2015-03-24 15:39:32','13','SOGC',3,'',10,1),(2,'2015-03-24 15:39:32','12','SOGC',3,'',10,1),(3,'2015-03-24 15:39:32','11','SOGC',3,'',10,1),(4,'2015-03-24 15:39:52','4','SOGC',2,'Changed date_to_remove.',10,1),(5,'2015-03-24 15:40:02','4','SOGC',2,'Changed date_to_remove.',10,1),(6,'2015-03-24 15:44:34','10','SOGC',3,'',10,1),(7,'2015-03-24 15:44:34','9','SOGC',3,'',10,1),(8,'2015-03-24 15:44:34','8','SOGC',3,'',10,1),(9,'2015-03-24 17:39:47','1','Submission Received - SOGC Job Bank',1,'',11,1),(10,'2015-03-25 14:09:50','14','SOGC',2,'Changed date_to_remove.',10,1),(11,'2015-03-25 16:22:07','14','SOGC',2,'Changed date_created.',10,1),(12,'2015-03-25 16:33:01','14','SOGC',2,'Changed date_created.',10,1),(13,'2015-03-25 16:33:12','14','SOGC',2,'Changed date_created.',10,1),(14,'2015-03-25 16:33:25','14','SOGC',2,'Changed date_created.',10,1),(15,'2015-03-25 16:37:56','14','SOGC',2,'Changed date_created.',10,1),(16,'2015-03-25 16:38:23','14','SOGC',2,'Changed date_created.',10,1),(17,'2015-03-25 16:38:40','14','SOGC',2,'Changed date_created.',10,1),(18,'2015-03-25 16:39:26','14','SOGC',2,'Changed date_created.',10,1),(19,'2015-03-26 13:22:09','3','SOGC',3,'',10,1),(20,'2015-03-26 13:22:41','5','SOGC',3,'',10,1),(21,'2015-03-26 13:50:03','15','SOGC',2,'Changed date_to_remove.',10,1),(22,'2015-03-26 13:50:08','15','SOGC',2,'No fields changed.',10,1),(23,'2015-03-26 13:56:05','14','SOGC',2,'Changed date_to_remove.',10,1),(24,'2015-03-27 17:42:12','15','DummyCorp',2,'Changed employment_type, other and posting_duration.',10,1),(25,'2015-03-30 13:42:34','4','DummyCorp',2,'Changed date_to_remove, employment_type and posting_duration.',10,1),(26,'2015-03-30 13:50:53','15','DummyCorp',2,'Changed title.',10,1),(27,'2015-03-30 13:51:13','14','DummyCorp',2,'Changed title, employment_type and posting_duration.',10,1),(28,'2015-03-30 13:51:49','7','IT Lab',2,'Changed date_to_remove, title, company_name, employment_type and posting_duration.',10,1),(29,'2015-03-30 13:52:18','14','SOGC',2,'Changed company_name.',10,1),(30,'2015-03-30 13:59:33','14','SOGC',2,'Changed date_to_remove.',10,1),(31,'2015-03-30 13:59:58','14','SOGC',2,'Changed date_to_remove.',10,1),(32,'2015-03-30 14:42:22','4','DummyCorp',2,'Changed title.',10,1),(33,'2015-03-30 14:42:44','6','DummyCorp',2,'Changed date_to_remove, title, employment_type and posting_duration.',10,1),(34,'2015-03-30 14:43:02','4','Design Lab',2,'Changed company_name.',10,1),(35,'2015-03-30 14:43:11','6','CPL Agency',2,'Changed company_name.',10,1),(36,'2015-03-30 14:43:52','2','DummyCorp',2,'Changed date_to_remove, title, employment_type and posting_duration.',10,1),(37,'2015-03-30 14:51:12','15','DoomedCorp',2,'Changed company_name.',10,1),(38,'2015-03-31 12:58:20','1','DummyCorp',3,'',10,1),(39,'2015-03-31 13:13:39','2','admin',1,'',3,1),(40,'2015-03-31 13:14:59','2','admin',2,'Changed first_name, last_name, email, is_staff and user_permissions.',3,1),(41,'2015-03-31 13:41:33','6','CPL Agency',2,'Changed date_to_remove.',10,2),(42,'2015-03-31 14:38:11','1','superadmin',2,'Changed username, first_name and last_name.',3,1),(43,'2015-04-02 14:19:08','4','Design Lab',2,'Changed city.',10,1),(44,'2015-04-02 14:19:18','6','CPL Agency',2,'Changed city.',10,1),(45,'2015-04-02 14:19:28','14','SOGC',2,'Changed city.',10,1),(46,'2015-04-02 14:19:42','15','DoomedCorp',2,'Changed city.',10,1),(47,'2015-04-02 14:20:26','7','IT Lab',2,'Changed city.',10,1),(48,'2015-04-02 14:20:59','2','DummyCorp',2,'Changed city.',10,1),(49,'2015-04-02 16:44:02','14','SOGC',2,'Changed employment_type, job_summary, roles_responsibilities, skills_competencies, education_experience and other.',10,1),(50,'2015-04-02 16:44:18','14','SOGC',2,'Changed start_date and deadline_date.',10,1),(51,'2015-04-02 16:44:54','14','SOGC',2,'Changed date_to_remove, start_date and deadline_date.',10,1),(52,'2015-04-09 13:28:48','17','SOGC',2,'Changed date_to_remove.',10,1),(53,'2015-04-10 18:18:16','19','SOGC',2,'Changed start_date and deadline_date.',10,1),(54,'2015-04-14 16:57:44','1','Submission Received - SOGC Job Bank',2,'Changed body.',11,2),(55,'2015-04-14 16:59:46','2','SOGC baque d\'emploi - Merci',1,'',11,2),(56,'2015-04-14 17:00:09','1','Submission Received - SOGC Job Bank',2,'Changed body.',11,2),(57,'2015-04-14 17:00:20','2','SOGC baque d\'emploi - Merci',2,'Changed code.',11,2),(58,'2015-04-14 17:00:39','1','Submission Received - SOGC Job Bank',2,'Changed code.',11,2),(59,'2015-04-14 17:00:53','2','SOGC baque d\'emploi - Merci',2,'Changed code.',11,2),(60,'2015-04-14 17:05:54','2','SOGC baque d\'emploi - Merci',2,'Changed body.',11,2),(61,'2015-04-14 17:06:05','2','SOGC baque d\'emploi - Merci',2,'Changed description.',11,2),(62,'2015-04-14 17:06:22','2','SOGC baque d\'emploi - Merci',2,'Changed body.',11,2),(63,'2015-04-14 17:10:08','3','SOGC: Confirmation of submission for Job Bank',1,'',11,2),(64,'2015-04-14 17:11:40','4','SOGC: Confirmation de soumission pour la Banque d’emploi',1,'',11,2),(65,'2015-04-14 17:11:49','2','SOGC baque d\'emploi - Merci',2,'Changed description.',11,2),(66,'2015-04-14 17:12:02','1','SOGC Job Bank: Submission Received',2,'Changed title.',11,2),(67,'2015-04-14 17:12:23','2','SOGC baque d\'emploi : Confirmation de soumission',2,'Changed title.',11,2);
/*!40000 ALTER TABLE `django_admin_log` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_content_type`
--

DROP TABLE IF EXISTS `django_content_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_content_type` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `app_label` varchar(100) NOT NULL,
  `model` varchar(100) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `django_content_type_app_label_45f3b1d93ec8c61c_uniq` (`app_label`,`model`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_content_type`
--

LOCK TABLES `django_content_type` WRITE;
/*!40000 ALTER TABLE `django_content_type` DISABLE KEYS */;
INSERT INTO `django_content_type` VALUES (1,'permission','auth','permission'),(2,'group','auth','group'),(3,'user','auth','user'),(4,'content type','contenttypes','contenttype'),(5,'session','sessions','session'),(6,'log entry','admin','logentry'),(8,'contact','jb','contact'),(9,'payment','jb','payment'),(10,'submission','jb','submission'),(11,'email','jb','email');
/*!40000 ALTER TABLE `django_content_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_migrations`
--

DROP TABLE IF EXISTS `django_migrations`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_migrations` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `app` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `applied` datetime NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_migrations`
--

LOCK TABLES `django_migrations` WRITE;
/*!40000 ALTER TABLE `django_migrations` DISABLE KEYS */;
INSERT INTO `django_migrations` VALUES (1,'contenttypes','0001_initial','2015-02-27 15:58:33'),(2,'auth','0001_initial','2015-02-27 15:58:33'),(3,'admin','0001_initial','2015-02-27 15:58:33'),(4,'jb','0001_initial','2015-02-27 15:58:34'),(5,'jb','0002_payment','2015-02-27 15:58:34'),(6,'jb','0003_payment_ccnumber','2015-02-27 15:58:34'),(7,'jb','0004_payment_ccsecurity','2015-02-27 15:58:34'),(8,'jb','0005_auto_20150227_1535','2015-02-27 15:58:34'),(9,'jb','0006_auto_20150227_1543','2015-02-27 15:58:34'),(10,'jb','0007_auto_20150227_1545','2015-02-27 15:58:34'),(11,'jb','0008_auto_20150227_1550','2015-02-27 15:58:35'),(12,'jb','0009_remove_position_deadline_date','2015-02-27 15:58:35'),(13,'jb','0010_auto_20150227_1556','2015-02-27 15:58:35'),(14,'jb','0011_contact_payment_position_submission','2015-02-27 15:58:35'),(15,'sessions','0001_initial','2015-02-27 15:58:35'),(16,'jb','0012_auto_20150227_1930','2015-02-27 19:30:20'),(17,'jb','0013_auto_20150302_1918','2015-03-02 19:18:48'),(18,'jb','0014_auto_20150302_1921','2015-03-02 19:21:08'),(19,'jb','0015_auto_20150302_2033','2015-03-03 18:58:51'),(20,'jb','0016_submission_date_to_remove','2015-03-03 18:59:11'),(21,'jb','0017_auto_20150305_1904','2015-03-05 19:04:40'),(22,'jb','0018_submission_renew','2015-03-11 19:14:20'),(23,'jb','0019_auto_20150317_1739','2015-03-17 17:39:15'),(24,'jb','0020_auto_20150318_1815','2015-03-18 18:16:03'),(25,'jb','0021_auto_20150319_1442','2015-03-19 14:42:26'),(26,'jb','0022_submission_payment','2015-03-19 14:51:04'),(27,'jb','0023_auto_20150319_1506','2015-03-19 15:07:02'),(28,'jb','0024_auto_20150319_1519','2015-03-19 15:19:56'),(29,'jb','0025_auto_20150319_1930','2015-03-19 19:30:23'),(30,'jb','0026_auto_20150319_1935','2015-03-19 19:35:55'),(31,'jb','0027_auto_20150323_1332','2015-03-23 13:33:07'),(32,'jb','0028_auto_20150323_1342','2015-03-23 13:42:58'),(33,'jb','0029_auto_20150323_1346','2015-03-23 13:46:45'),(34,'jb','0030_auto_20150324_1723','2015-03-24 17:23:59'),(35,'jb','0031_auto_20150325_1635','2015-03-25 16:35:21'),(36,'jb','0032_auto_20150325_1641','2015-03-25 16:41:08'),(37,'jb','0033_auto_20150327_1728','2015-03-27 17:34:56'),(38,'jb','0034_remove_submission_displayed','2015-03-27 17:37:19'),(39,'jb','0035_auto_20150330_1913','2015-03-30 19:13:30'),(40,'jb','0002_auto_20150415_1350','2015-04-15 13:51:01');
/*!40000 ALTER TABLE `django_migrations` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `django_session`
--

DROP TABLE IF EXISTS `django_session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `django_session` (
  `session_key` varchar(40) NOT NULL,
  `session_data` longtext NOT NULL,
  `expire_date` datetime NOT NULL,
  PRIMARY KEY (`session_key`),
  KEY `django_session_de54fa62` (`expire_date`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `django_session`
--

LOCK TABLES `django_session` WRITE;
/*!40000 ALTER TABLE `django_session` DISABLE KEYS */;
INSERT INTO `django_session` VALUES ('b5an9dgcppafx8xpsubdg2g0a54vns51','NDkzNDFhZGJmZTRmMDhmMmYzMjIzNDAxZWNkNjc2MDE3ZDNkNDJlYzp7Il9hdXRoX3VzZXJfaGFzaCI6ImM3ODNlYWQxNjRiN2Y5NzVlOTRlZmEwNzhhZTk2N2NkNjNiOTUzN2UiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjJ9','2015-04-14 13:15:17'),('hw3rs9g7g5axlsf5kxl2apxbyqfs5zi7','NzU3OTJlMjJmMmNhMzcwNTRlMmFhYmRhNDk4ODcyZGU1ZWVhNDZhNTp7Il9hdXRoX3VzZXJfaGFzaCI6IjFkNWE5ZGIzNjM5MDFiZWQyN2UzYWQ5NjFmMWU4NTZlNTBhOWUyZDgiLCJfYXV0aF91c2VyX2lkIjoxLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCJ9','2015-04-14 14:38:28'),('ruda986xqeiidgefp7etvmsei6lj3bvh','MzhiZDZkY2FlNWU4ZjliYjVlN2VlMDE0Yzk1Yzg5NTFkZDkwZTNiYTp7Il9hdXRoX3VzZXJfaGFzaCI6IjJiYTAxZGFmMDBhYmZlNzI4ZjljMTc5ZmU1MDMzMDgyMjg3MWIxODYiLCJfYXV0aF91c2VyX2JhY2tlbmQiOiJkamFuZ28uY29udHJpYi5hdXRoLmJhY2tlbmRzLk1vZGVsQmFja2VuZCIsIl9hdXRoX3VzZXJfaWQiOjF9','2015-04-30 13:09:26');
/*!40000 ALTER TABLE `django_session` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jb_contact`
--

DROP TABLE IF EXISTS `jb_contact`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jb_contact` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(255) NOT NULL,
  `last_name` varchar(255) NOT NULL,
  `email` varchar(254) NOT NULL,
  `phone` varchar(50) NOT NULL,
  `address1` varchar(255) NOT NULL,
  `address2` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL,
  `province` varchar(255) NOT NULL,
  `postal_code` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=29 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jb_contact`
--

LOCK TABLES `jb_contact` WRITE;
/*!40000 ALTER TABLE `jb_contact` DISABLE KEYS */;
INSERT INTO `jb_contact` VALUES (1,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(2,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(3,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(4,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(5,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','2015-03-27 17:27:04.330471+00:00','Ontario','K1S5G8','DummyCorp'),(6,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(7,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(8,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(9,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(10,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(11,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(12,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(13,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(14,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(15,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(16,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(17,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(18,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','2015-03-27 17:26:21.433127+00:00'),(20,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(21,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','ON','K1S5G8','SOGC'),(22,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G7','SOGC'),(23,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(24,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(25,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(26,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(27,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC'),(28,'Salma','Nassri','snassri@sogc.com','6132456789','780 Echo Drive','','Ottawa','Ontario','K1S5G8','SOGC');
/*!40000 ALTER TABLE `jb_contact` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jb_email`
--

DROP TABLE IF EXISTS `jb_email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jb_email` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `code` varchar(50) NOT NULL,
  `title` varchar(255) NOT NULL,
  `body` longtext NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `code` (`code`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jb_email`
--

LOCK TABLES `jb_email` WRITE;
/*!40000 ALTER TABLE `jb_email` DISABLE KEYS */;
INSERT INTO `jb_email` VALUES (1,'sub-confirmation','SOGC Job Bank: Submission Received','Dear {{ contact.first_name }} {{ contact.last_name }},\r\n<p>\r\nThank you for your submission. Once the payment has been processed, usually within 2-3 business days, your posting will appear in the Job Bank.\r\n</p>\r\n<p>To edit your posting, please follow this link: <a href=\"{% url \'jb:edit_submission\' job.uuid %}\"> {% url \'jb:edit_submission\' job.uuid %}</a>  Please remember that after 30 days, you will not be able to make any changes to your posting.\r\n</p>\r\n<p>\r\nIf you should have any questions or are seeking additional information, please contact Linda Kollesh at 613-730-4192, Ext. 233 or by email at lkollesh@sogc.com.\r\n</p>\r\n','Automatic email received by submitters as confirmation of receipt'),(2,'sub-confirmation-FR','SOGC baque d\'emploi : Confirmation de soumission','Bonjour {{ contact.first_name }} {{ contact.last_name }},\r\n<p>\r\nMerci pour votre soumission. Dès que le paiement sera traité, habituellement en deux à trois jours ouvrables, votre annonce apparaîtra dans la banque d\'emplois.\r\n</p>\r\n<p>Pour modifier votre annonce, veuillez suivre ce lien: <a href=\"http://jobs.sogc.org/jb/fr/{{job.uuid}}\"> http://jobs.sogc.org/jb/fr/{{job.uuid}} </a> Veuillez prendre note, qu’après 30 jours de la date de soumission initiale, vous ne serez pas en mesure d\'apporter des modifications à votre annonce.\r\n</p>\r\n<p>\r\nSi vous avez des questions ou besoin de renseignements supplémentaires, veuillez communiquer avec Linda Kollesh au <nobr>613-730-4192, poste 329</nobr>, ou par courriel à  <a href=\"mailto:lkollesh@sogc.com\">lkollesh@sogc.com</a>.</p>\r\n','Automatic email received by submitters as confirmation of receipt - French'),(3,'processed-inform','SOGC: Confirmation of submission for Job Bank','<p>This is to confirmed that your submission was processed and it now appears on our Job Bank webpage: http://jobs.sogc.org/job-bank/.</p>\r\n \r\n<p>Please note that your official invoice and receipt will be emailed to you shortly.</p>\r\n \r\n<p>Should you require further information, please do not hesitate to contact us at membership@sogc.com.</p>\r\n \r\n<p>Have a great day!</p>','Sent to submitter automatically once the submission is processed '),(4,'processed-inform-FR','SOGC: Confirmation de soumission pour la Banque d’emploi','<p>Ceci est pour confirmer que votre soumission a été traité et apparaît maintenant sur notre site de Banques d’emploi : <a href=\"http://jobs.sogc.org/job-bank\">http://jobs.sogc.org/job-bank</a> .\r\n\r\n<p>Veuillez prendre note que votre facture officielle ainsi que reçu seront envoyés par courriel sous peu.</p>\r\n\r\n<p>Si vous avez besoin de plus amples informations, n’hésitez pas à communiquer avec nous à membership@sogc.com.</p>\r\n\r\n<p>Bonne journée!</p>\r\n ','Sent to submitter automatically once the submission is processed - French');
/*!40000 ALTER TABLE `jb_email` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jb_payment`
--

DROP TABLE IF EXISTS `jb_payment`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jb_payment` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `ccType` varchar(50) NOT NULL,
  `ccNumber` longtext,
  `ccHolderName` varchar(255) DEFAULT NULL,
  `ccSecurity` longtext,
  `ccExpiry` date NOT NULL,
  `posting_duration` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jb_payment`
--

LOCK TABLES `jb_payment` WRITE;
/*!40000 ALTER TABLE `jb_payment` DISABLE KEYS */;
INSERT INTO `jb_payment` VALUES (1,'visa','AMVDMgF0YxYXlcwrOU2tZOad4AC46_jnawH2pt7MrjfQR5zvh1Kg0YXUp_ITDCBEtDobYeUM7MQL','adfdsfd','AMVDMgFOerTWPwLIAcUaJjozIMrWkQIfVxx9IhABJfx462YP0PqRJUrqMw2YhOj2WPJXLbsYBK2J','2015-03-23',NULL),(2,'visa','AMVDMgEbQZBgDesGoOH8IlreEpttvnaD2xjGiF84I_fw3ek7yQnn-r3_7-YLb5cJv3bx_cKZTdsRpgjy8Y-dkyYu2GehO-h5AQ','Salma Nassri','AMVDMgHPKtSRBrUHPaY402L2bX3YLk4ZYacLUg4YChmPR1N-B0clOJP3bswhDBCukxWxmpoTvjn6','2012-06-01',NULL),(3,'visa','AMVDMgGB0L9v4P9bsg4SeJq2kN3mIq3p_JtcZI39CpxyesvWuvA2vOFFvCHEK9gXIwB8NXGGw9m6yxR0ogTqMWur9qZF3Ynk4A','Salma Nassri','AMVDMgHJAxwvlfftJggMwcjgTEP3qPuCO_Cha_AAfy2Hh6HjW5ZdnOi2ysGC6cD6EVtyDdT1uFRi','2012-06-01',NULL),(4,'visa','AMVDMgEdFLfQ-Gw0wAOaB-xRYjeHr_M8oPEjIZAzpJXfgiMOM9_O6mjU56VNBJnx8MrbfObl2kfWXqFYkipAgFi39-oruv7ssA','Salma Nassri','AMVDMgHVaxkwTyUU07MjjhmrNoohrBD-8N3qAr9PeqZsmS7CI5hwN6Jgs8NaZjvru3pL0AU-LWpb','2012-06-01',NULL),(5,'visa','AMVDMgEvZOyrkGNQEfADrhkAajisFOA0mjhYr5QjoDLnYN9y48SLrzrs5JB8hBbzmc7rqJLoCWAdvc2NP8QZGOnVHU9cx5LkyA','Salma Nassri','AMVDMgEHwhITyVn7wqoSzT38dw7ELXRNuJuLj7H7zUq0I5ugkBRn3n0CV3x60uVQTkv6WHFCGdDT','2012-06-01',NULL),(6,'visa','AMVDMgE5H_YgJfnlNdNHUjmv8OyanIAbrylWe83mzn3iHbyVzYEJKAR8rAV8wGomd62P7ujCjnRjCKxk-TB8fM0KhANOSUuGzg','Salma Nassri','AMVDMgGf2auDM-YE8226pbGPDdHtB6c2Cd0yqwnDmG1UOZi6nGRyWtRrVrP5D1jqE2hxjkuIKfUf','2012-06-01',NULL),(7,'visa','AMVDMgE2Lcpqm2sfylE5JVuCroOgQCSWVyBKOtM8_uljRszzd-t6lHtfPxkSwlLQxIiuUpg39-JkynDrwcJQQi55lsHK3BbWXA','Salma Nassri','AMVDMgHintn_ou89HbZ3AU8DMAZ0HvmrVlsevpjGkILqjh5qOAtZXXbUcB5f5ObqxFP1-W9iifnx','2012-06-01',NULL),(8,'visa','AMVDMgFfXQG5zjmGclzQ3HgA4SmxadTE2CIsC6ek-3SaI7P7Fwi5zbc14Xhy3TwfDcZVK4qpJe0iDhNCe05fQ9_brd2_vQhcvQ','Salma Nassri','AMVDMgFZx2RgZuEmE41dAJt5fhVuOrVOzdAAZPd-s6faX5gRs-qQzuuls9VTjwlzBLpM1wu33QIR','2012-06-01',NULL),(9,'visa','AMVDMgHJ_YzTxKyDbfxiXZb0v2udfC6O0CB_STEEPhyow-tUXGZFfb5xjybc59bsCxGoHWZRQ-ay2PXvk4BF1gWpT5cbNb0NpA','Salma Nassri','AMVDMgGL1KMy1hxsRPIT1FhvVsZxcBiVucRQtaGTJcp29bSlnvSkd-T-RDLdsYBCxVb9yQbOAj_k','2012-06-01',NULL),(10,'visa','AMVDMgFEF2G9qmzTcevvQueRj9SbMHMGTjVtOGhhVseyBw3N_5Jqw7xl9x_of3YjpA7SrD5k8XWW2N9ePVpRmDkyxySazJUpSA','Salma Nassri','AMVDMgFX5YRZkZ6N2ALo9JhQ5DzMt7KXbGk5httl9keWTBwqZE09nCYw3aHjppJlxhT5kmL1RaIR','2012-06-01',NULL),(11,'visa','AMVDMgGN_FDq3Fm36r5WGuxXnYfE5DtQc7EKy58PDsYeqm4S8pz4xK_DKrJFnGOa_hkXZJbgJJ4bNlmMITb9uOc4Z0x-zCSWcA','Salma Nassri','AMVDMgHptlA6n28ErQeyyNsWyk2rSr86f79MdJKHYTqDzDJNffwH8BUqY3jkfxrbB5DHGalSjRmv','2012-06-01',NULL),(12,'visa','AMVDMgEhxr93ZoQ-gSX9T8eOqxOqjRqEH8PH-nnQrlQUhlID1Pq3tVqAhM6UW8G0dic5OY_mGJbv4B3mdxIcMWFQvyU5e2-tJQ','Salma Nassri','AMVDMgFsuHMYZjVCAI1OhJA9BHGimtWUolBQOOAfcOTlIbADfPnsHs1yIpDhdjDDFdwsFITMXwqq','2012-06-01',NULL),(13,'visa','AMVDMgH7U1WbDHtSiTabjF4QDOi4toEdXnCzMR6faRyo4T9yAiXtdxkccmBE-vDbuIL5dqOZA_LGXenmNQLp36c7ocWOV1i_2g','Salma Nassri','AMVDMgHDTS12HLOhE6pxZGa8IEq--yrlhjO61MaHwK87_2ddH5ZKJesEOsUZXuMQPW97YkhfvAfY','2012-06-01',NULL),(14,'visa','AMVDMgEfGUjiB6tRKxc17VN5tytocnCKgXNlqvVmYsxRhtW7pGf36CFHQtCao6yA72r8LaHgqJ_3M6TDSHCm_wfjrN9KLc215Q','Salma Nassri','AMVDMgF9DPiXPWetOM_pLwK9gZaB1Km8hs02ZOeJPt_8UedGsrE7T9wMyKbGIQkHN_pNE9-iLTkM','2012-06-01',NULL),(15,'visa','AMVDMgFpfps2pz1TGbmMRSjyy_VYrv-Ln1TvO_lzw4Wiv1mFUOQmqhQ25emddLvFb0Z3Zy3LspXZgyHuVK7fsxvSnvh8MNLGQQ','Salma Nassri','AMVDMgEsTYnv4AvrPkqFj-iwNKZYxs-BehRZBuJ43J7KfjhMx1W7ffEGustZB3S9e2FnPAOpK9Q2','2012-06-01',NULL),(16,'visa','AMVDMgHyiaaCdAMk4DytCHYoCqt_ApxxjZ43KxruYQ0-bKkbUMzaVcUeQHdJTGucUNgTm0v4GxS_1Zbm0QetGUPHqNHW6cnlUg','Salma Obaid','AMVDMgEgX9zrATVOnBoYsQIIZU9W03EpOmOvqYdIGzIrFrV18sn8ATwK7ufZCWHDTyha4PBqujyT','2012-06-01',NULL),(17,'visa','AMVDMgEfFclRn0-13YrblgJPmEcq0GcIF5uBepljIxIxiwLvynhmKji9UWvFWMkbHs470Qm2D6eJMHcH0DxcNOmog_BQdRsZ6w','Salma Nassri','AMVDMgFI9tthU43byvjsoet0sZamNUndL_UG-PINlsd2HMjoR6yKrzaH8CUclhj7el9ZQ6oqaxmZ','2012-06-01',3),(18,'visa','AMVDMgGLm7-86XecvryVo5bvlr9oWft22MSuZglXpDCKsxKcmGptrkC8KNFzwrd3UGJL6s0ji4gFI-gundWPnNEmDF-T-p5tvw','Salma Nassri','AMVDMgFVJx33691CFEg3qIbHR5AeRyjdFtrbTEev8d0GGrujwoTjRAUmtZL1u-mUJJhq2Bg6VYG3','2012-06-01',1),(19,'mastercard','AMVDMgGdZiriyyJkBf0HC7TJmLF59N3A9aGmsdtpQzBlfVMPdxxM58rBruRm5sB-UAA6fyFuR0TxqSsYoS22M4caAP2vjpqE3Q','Salma Nassri','AMVDMgGhmKq4deq_HqJ02PbWzopWTgqgmEKziS_75ktEbSqeR15YgO35CtUAGd4wZld1oRhtHWsh','2012-06-01',1),(20,'mastercard','AMVDMgEa5buL5XV3jL4Z6oM26HQvbbLS4Ntsw9K0XzOLmIy_D5W4ca_Nb1mTsz6j2EJaC0L5k72AcwSffpwe1LVRNPcrBubKag','Salma Nassri','AMVDMgHCAUTQv5izh_yLePyGRK4hLBL6islMkDtjOP3Pwt7Hvx5Ja16ifgoNMr-v68kTN-sR28G2','2012-06-01',2),(21,'visa','AMVDMgHKkw21YH518lbawd-hQuFn46eBlMTJ1RyQRh7zUINpg56_l-L9LTO7Y43AoRtg5bmKOEnzshJ7fGB3PL3WTy9217C1UA','Salma Nassri','AMVDMgEZOVYAEtalUE7nNbqJA8ZlsY7Qiw0bp1bDoUzss8KznKgpy-LIdLDcE657E-ugszL2YfSW','2012-06-01',1),(22,'mastercard','AMVDMgFCnAxHPzauYtBAYgZ6DVxQOTA-OHUugKo8IKr-pOYDxQtFbVBsPWR1yFgFasepSkUNveQYfCKWwnFODKw6jif_8zKSbQ','Salma Nassri','AMVDMgGhz0VZSMnsEiVzZFzM9718WBtfqUFnGMhCjzyOk3_uHGeIOgGiqHaSiEUVPH5ZZ-Zd5thT','2012-06-01',2),(23,'mastercard','AMVDMgFwLdEPE5nNOj1gXM1ttcmyG36Ze8FMvTbrsZgvnVijzSFznil-7MHn65V9EtCmxl4dgEAw1TM2KAlSBkrDz8s0WvUGVQ','Salma Nassri','AMVDMgF8hK0GdM4uQBiYiZ9jt4RbZmEPcJn-qShQoiqLKJHHzI51_Jh6ZgmFVsPgbrKyQnEih0b6','2012-06-01',2),(24,'visa','AMVDMgFoEA0xdtcW2a0p3eFbc9BC8yN0FHHvfxNTj7QdCzm6E-C1qm8svYK2r9HpgQuCqmRaLkkqAxERyBz9g3Hk9sUQuxirpw','Salma Nassri','AMVDMgGaGxoIkUWWk0EOvqw1gEWpaZnlEpAxuXQpNB_g4YCMZVkYjzo_a9u7I-JQ7uEnnBfAiwQO','2012-06-01',2);
/*!40000 ALTER TABLE `jb_payment` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `jb_submission`
--

DROP TABLE IF EXISTS `jb_submission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `jb_submission` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `language` varchar(50) NOT NULL,
  `processed` tinyint(1) NOT NULL,
  `date_created` datetime NOT NULL,
  `date_modified` datetime NOT NULL,
  `contact_id` int(11) NOT NULL,
  `uuid` char(32) NOT NULL,
  `date_to_remove` date DEFAULT NULL,
  `payment_id` int(11) NOT NULL,
  `city` varchar(255) NOT NULL,
  `company_name` varchar(255) NOT NULL,
  `contract_type` varchar(255) NOT NULL,
  `country` varchar(255) NOT NULL,
  `deadline_date` datetime DEFAULT NULL,
  `division` varchar(255) NOT NULL,
  `education_experience` longtext NOT NULL,
  `employment_type` varchar(255) NOT NULL,
  `job_summary` longtext NOT NULL,
  `other` longtext NOT NULL,
  `position_type` varchar(255) NOT NULL,
  `posting_duration` int(11) DEFAULT NULL,
  `province` varchar(255) NOT NULL,
  `reference_num` varchar(255) NOT NULL,
  `roles_responsibilities` longtext NOT NULL,
  `salary_details` varchar(255) NOT NULL,
  `salary_max` int(11) NOT NULL,
  `salary_min` int(11) NOT NULL,
  `salary_negotiable` tinyint(1) NOT NULL,
  `skills_competencies` longtext NOT NULL,
  `start_date` datetime DEFAULT NULL,
  `title` varchar(255) NOT NULL,
  `url` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `contact_id` (`contact_id`),
  UNIQUE KEY `uuid` (`uuid`),
  UNIQUE KEY `payment_id` (`payment_id`),
  CONSTRAINT `jb_submission_contact_id_2b8c5ae0d9fd2193_fk_jb_contact_id` FOREIGN KEY (`contact_id`) REFERENCES `jb_contact` (`id`),
  CONSTRAINT `jb_submission_payment_id_34af36856c5abc07_fk_jb_payment_id` FOREIGN KEY (`payment_id`) REFERENCES `jb_payment` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=25 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `jb_submission`
--

LOCK TABLES `jb_submission` WRITE;
/*!40000 ALTER TABLE `jb_submission` DISABLE KEYS */;
INSERT INTO `jb_submission` VALUES (2,'en',0,'2015-03-23 14:31:42','2015-04-02 14:20:59',5,'531f1bb24ac347bfb37b6d39349eda4a','2015-03-30',2,'Dummy City','DummyCorp','Locum','Canada',NULL,'','','Parttime','','','Academic',3,'Ontario','1','','',0,0,0,'',NULL,'Marketing Agency',''),(4,'en',1,'2015-03-24 15:12:50','2015-04-02 14:19:08',7,'542377f25ebd41fbaf5ac566d3254bcd','2015-04-08',4,'Toronto','Design Lab','Locum','Canada',NULL,'','','Fulltime','','','Academic',1,'Ontario','1','','',0,0,0,'',NULL,'Graphic Designer',''),(6,'en',1,'2015-03-24 15:13:58','2015-04-02 14:19:18',9,'1ad375ee1b0d4bb7b34f0955ea4153b3','2015-04-22',6,'Hamilton','CPL Agency','Locum','Canada',NULL,'','','Fulltime','','','Academic',1,'Ontario','1','','',0,0,0,'',NULL,'Events Coordinator',''),(7,'en',1,'2015-03-24 15:14:10','2015-04-02 14:20:26',10,'846376137bc144a2a95b5c236273bc93','2015-04-30',7,'Oshawa','IT Lab','Locum','Canada',NULL,'','','Fulltime','','','Academic',1,'Ontario','1','','',0,0,0,'',NULL,'Interactive Designer',''),(14,'en',1,'2015-02-22 15:47:56','2015-04-02 16:44:54',17,'804c54cebf5e41ddaa9686c062cdce0a','2015-04-16',14,'New Orleans','SOGC','Locum','Canada','2015-04-02 00:00:00','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','Academic',1,'Ontario','1','Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','',0,0,0,'Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.','2015-04-30 00:00:00','Database Administrator',''),(15,'en',1,'2015-03-26 13:43:16','2015-04-02 14:19:42',18,'701bf8fefb4948fb9102fc5f479e1a5d','2015-04-30',15,'Some City','DoomedCorp','Locum','Canada',NULL,'','','Fulltime','','Lorem ipsum dolor sit amet. ','Academic',1,'Ontario','1','','',0,0,0,'',NULL,'Web Developer',''),(16,'en',1,'2015-03-30 19:13:38','2015-03-30 19:39:12',20,'1db2466ceb97402dbcfc87dbb3b1fceb','2015-04-30',16,'Ottawa','SOGC','Indeterminate/permanent','Canada',NULL,'','None required','Parttime','A very cool job ','','Other',1,'Ontario','','None','',0,0,0,'None',NULL,'Interactive Developer',''),(17,'en',1,'2015-04-09 13:17:34','2015-04-10 18:13:00',21,'b38d3ba280094c58b4fdfffee2a465c1','2015-07-10',17,'Ottawa','SOGC','Indeterminate/permanent','Canada','2013-09-09 00:00:00','','','Full-time','','','Administrative',1,'ON','','','',0,0,0,'','2011-09-30 00:00:00','Web Developer',''),(18,'en',1,'2015-04-10 17:12:12','2015-04-10 17:12:12',22,'eff670e079c246fa9e9fdb4fd2edeb6d','2015-05-10',18,'Ottawa','SOGC','','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Web Developer',''),(19,'en',1,'2015-04-10 18:11:15','2015-04-10 18:18:16',23,'fb9d4af9736141f191ab84ad819af62a','2015-05-10',19,'Ottawa','SOGC','','Canada','2015-04-30 00:00:00','','','','','','',NULL,'Ontario','','','',0,0,0,'','2015-04-10 00:00:00','Web Developer','http://sogc.org/events/acsc-2015/scientific-program/'),(20,'en',0,'2015-04-14 15:14:07','2015-04-14 15:14:07',24,'b497f4c1c84f4af8b9f6119bc61a86d6',NULL,20,'Ottawa','SOGC','Indeterminate/permanent','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Web Developer',''),(21,'en',0,'2015-04-14 15:14:37','2015-04-14 15:14:37',25,'e33dba72f267474098fdb2ef1bc81957',NULL,21,'Ottawa','SOGC','','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Database Admin',''),(22,'en',0,'2015-04-14 15:45:50','2015-04-14 15:45:50',26,'9b08099fea9c4c9698fafef0ba7cf91a',NULL,22,'Ottawa','SOGC','Indeterminate/permanent','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Web Developer',''),(23,'en',0,'2015-04-14 15:46:16','2015-04-14 15:46:16',27,'ea4bbbb720a749a282cc1c8bd6c2e627',NULL,23,'Ottawa','SOGC','Indeterminate/permanent','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Web Developer',''),(24,'en',0,'2015-04-14 15:46:43','2015-04-14 15:46:43',28,'0be715dd35ad4609a88c7afac7df0ea2',NULL,24,'Ottawa','SOGC','','Canada',NULL,'','','','','','',NULL,'Ontario','','','',0,0,0,'',NULL,'Web Developer','');
/*!40000 ALTER TABLE `jb_submission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-04-21 15:37:41
