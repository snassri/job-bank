from django.conf.urls import patterns, include, url
from django.contrib import admin
from jb import views as jb_views
from django.conf import settings

admin.autodiscover()

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'jobbank.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^jb/', include('jb.urls', namespace="jb")),
    url(r'^job-bank/(?P<lang>[a-zA-Z]{2})/$', jb_views.jobs_list),
    
    url(r'^static/(?P<path>.*)$', 'django.views.static.serve', {'document_root': settings.STATIC_ROOT}),
)
