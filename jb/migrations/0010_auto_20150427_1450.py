# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0009_auto_20150423_1732'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='country',
            field=models.CharField(default='Canada', max_length=255),
            preserve_default=False,
        ),
        migrations.AlterField(
            model_name='contact',
            name='province',
            field=models.CharField(max_length=255, verbose_name='Province / Territory / State'),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='submission',
            name='province',
            field=models.CharField(max_length=255, verbose_name='Province/ Territory / State'),
            preserve_default=True,
        ),
    ]
