# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0015_contact_show_contact'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='show_contact',
        ),
        migrations.AddField(
            model_name='submission',
            name='show_contact',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
