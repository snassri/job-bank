# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0014_auto_20150515_1801'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='show_contact',
            field=models.BooleanField(default=True),
            preserve_default=True,
        ),
    ]
