# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0010_auto_20150427_1450'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='contact',
            name='country',
        ),
    ]
