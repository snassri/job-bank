# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0012_contact_country'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='slug',
            field=models.SlugField(unique=True),
            preserve_default=True,
        ),
    ]
