# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import encrypted_fields.fields
import uuidfield.fields


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Contact',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('first_name', models.CharField(max_length=255, verbose_name='First Name')),
                ('last_name', models.CharField(max_length=255, verbose_name='Last Name')),
                ('email', models.CharField(max_length=254, verbose_name='Email')),
                ('phone', models.CharField(max_length=50, verbose_name='Phone', blank=True)),
                ('address1', models.CharField(max_length=255, verbose_name='Address 1')),
                ('address2', models.CharField(max_length=255, verbose_name='Address 2', blank=True)),
                ('city', models.CharField(max_length=255, verbose_name='City')),
                ('province', models.CharField(max_length=255, verbose_name='Province / Territory')),
                ('postal_code', models.CharField(max_length=255, verbose_name='Postal Code')),
                ('company_name', models.CharField(max_length=255, verbose_name='Company Name', blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Email',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('code', models.CharField(unique=True, max_length=50)),
                ('title', models.CharField(max_length=255)),
                ('body', models.TextField()),
                ('description', models.CharField(max_length=255, null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Payment',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('posting_duration', models.IntegerField(null=True, verbose_name='Posting Duration (in months)', choices=[(1, b'1'), (2, b'2'), (3, b'3')])),
                ('ccType', models.CharField(max_length=50, verbose_name='Credit Card Type', choices=[(b'visa', b'Visa'), (b'mastercard', b'MasterCard')])),
                ('ccNumber', encrypted_fields.fields.EncryptedCharField(max_length=255, verbose_name='Credit Card Number')),
                ('ccHolderName', models.CharField(max_length=255, verbose_name='Name of Card Holder')),
                ('ccExpiry', models.DateField(verbose_name='Credit Card Expiry Date (mm/yy)')),
                ('ccSecurity', encrypted_fields.fields.EncryptedCharField(max_length=50, verbose_name='Credit Card Security Code')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Submission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('uuid', uuidfield.fields.UUIDField(unique=True, max_length=32, editable=False, blank=True)),
                ('language', models.CharField(default=b'e', max_length=50, choices=[(b'e', b'English'), (b'f', b'French')])),
                ('processed', models.BooleanField(default=False)),
                ('date_created', models.DateTimeField(auto_now_add=True)),
                ('date_modified', models.DateTimeField(auto_now=True)),
                ('date_to_remove', models.DateField(null=True, verbose_name=b'Date of Removal')),
                ('title', models.CharField(max_length=255, verbose_name='Position Title')),
                ('company_name', models.CharField(max_length=255, verbose_name='Company Name')),
                ('division', models.CharField(max_length=255, verbose_name='Division', blank=True)),
                ('reference_num', models.CharField(max_length=255, verbose_name='Reference Number', blank=True)),
                ('url', models.CharField(max_length=255, verbose_name='Job Posting URL', blank=True)),
                ('city', models.CharField(max_length=255, verbose_name='City')),
                ('province', models.CharField(max_length=255, verbose_name='Province')),
                ('country', models.CharField(max_length=255, verbose_name='Country')),
                ('salary_min', models.IntegerField(default=0, max_length=50, verbose_name='Salary Minimum', blank=True)),
                ('salary_max', models.IntegerField(default=0, max_length=50, verbose_name='Salary Maximum', blank=True)),
                ('salary_details', models.CharField(max_length=255, verbose_name='Salary Details', blank=True)),
                ('salary_negotiable', models.BooleanField(default=False, verbose_name='Salary Negotiable')),
                ('employment_type', models.CharField(blank=True, max_length=255, verbose_name='Type of Employment', choices=[(b'Full-time', 'Full-time'), (b'Part-time', 'Part-time')])),
                ('contract_type', models.CharField(blank=True, max_length=255, verbose_name='Type of Contract', choices=[(b'Indeterminate/permanent', 'Indeterminate/permanent'), (b'Term/contract', 'Term/contract'), (b'Locum', 'Locum')])),
                ('position_type', models.CharField(blank=True, max_length=255, verbose_name='Type of Position', choices=[(b'Academic', 'Academic'), (b'Administrative', 'Administrative'), (b'Obstetrics', 'Obstetrics'), (b'Gynaecology', 'Gynaecology'), (b'Obstetrics/Gynaecology', 'Obstetrics/Gynaecology'), (b'Other', 'Other')])),
                ('job_summary', models.TextField(verbose_name='Job Summary', blank=True)),
                ('roles_responsibilities', models.TextField(verbose_name='Roles and Responsibilities', blank=True)),
                ('skills_competencies', models.TextField(verbose_name='Skills and Competencies', blank=True)),
                ('education_experience', models.TextField(verbose_name='Education and Experience', blank=True)),
                ('other', models.TextField(verbose_name='Other', blank=True)),
                ('start_date', models.DateField(null=True, verbose_name='Start Date of Employment (dd/mm/yyyy)', blank=True)),
                ('deadline_date', models.DateField(null=True, verbose_name='Deadline Date of Application (dd/mm/yyyy)', blank=True)),
                ('contact', models.OneToOneField(to='jb.Contact')),
                ('payment', models.OneToOneField(to='jb.Payment')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
