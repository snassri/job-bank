# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0005_auto_20150422_1421'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='submission',
            name='slug',
        ),
    ]
