# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='language',
            field=models.CharField(default=b'en', max_length=50, choices=[(b'en', b'English'), (b'fr', b'French')]),
            preserve_default=True,
        ),
    ]
