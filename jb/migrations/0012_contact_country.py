# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0011_remove_contact_country'),
    ]

    operations = [
        migrations.AddField(
            model_name='contact',
            name='country',
            field=models.CharField(default='Canada', max_length=255),
            preserve_default=False,
        ),
    ]
