# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('jb', '0003_submission_slug'),
    ]

    operations = [
        migrations.AlterField(
            model_name='submission',
            name='slug',
            field=models.SlugField(),
            preserve_default=True,
        ),
    ]
