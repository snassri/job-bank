import itertools
from django.utils.text import slugify
from django.shortcuts import render
from django.shortcuts import render, render_to_response, redirect, get_object_or_404
from django.template import RequestContext, Context, Template
from jb.forms import PositionForm, ContactForm, PaymentForm
from jb.models import Submission, Email
from django.utils import translation
from django.utils.html import strip_tags, format_html
from django.core.mail import EmailMultiAlternatives, send_mail
import datetime



def activate_language(lang="en"):
	language = lang
	if language=="fr":
		translation.activate("fr")
	else:
		translation.activate("en")
	return language
	

def jobs_list(request, lang="en"):
    
    language = activate_language(lang)
        
    #get all submissions with displayed = True 
    #also need to automatically remove posts that are past the removal date (or check if it's past the date)
    
    displayed_jobs = Submission.objects.filter(processed=True)
    
    
    #filter jobs based on if it is past the removal date
    filtered_jobs = [job for job in displayed_jobs if not job.is_past_due]
    
    ## an alternate method to the above is to automatically change "processed" to False if the date is passed the removal date, upon calling the Submission

    return render(request, 'jb/mainlist.html', { 'jobs':filtered_jobs })
    
    
def job_detail(request, post_slug, lang="en"):
    
    language = activate_language(lang)
    
    the_submission = get_object_or_404(Submission, slug=post_slug)
    
    closed = False
    passed = the_submission.is_past_due
    processed = the_submission.processed
    
    if passed or not processed:
        closed = True
    
    return render(request, 'jb/jobdetail.html', { 'job':the_submission, 'closed':closed })
    


def submission_form(request, lang="en"):
    
    language = activate_language(lang)
	
    if request.method == 'POST':
        positionform = PositionForm(request.POST)
        contactform = ContactForm(request.POST)
        paymentform = PaymentForm(request.POST)

        if positionform.is_valid() and contactform.is_valid() and paymentform.is_valid() :
            
            new_contact = contactform.save()
            new_payment = paymentform.save()
            new_submission = positionform.save(commit=False)

            new_submission.slug = orig = slugify(new_submission.title)

            for x in itertools.count(1):
			    if not Submission.objects.filter(slug=new_submission.slug).exists():
				    break
			    new_submission.slug = '%s-%d' % (orig, x)

            new_submission.contact = new_contact
            new_submission.payment = new_payment
            new_submission.language = language
            new_submission.save()
            
            #uncomment these lines when in production
            
            '''
            if language == "fr":
                send_email(new_submission.contact, "sub-confirmation-FR", new_submission)
            else:
                send_email(new_submission.contact, "sub-confirmation", new_submission)
            '''
            return render(request, 'jb/thankyou.html', { 'job': new_submission } )
            
        else:
            return render(request, 'jb/submissionform.html', {'contactform':contactform, 'positionform': positionform, 'paymentform': paymentform })	
    else:
        contactform = ContactForm
        positionform = PositionForm
        paymentform = PaymentForm
        return render(request, 'jb/submissionform.html', { 'contactform': contactform, 'positionform': positionform, 'paymentform': paymentform })
        

def edit_submission(request, uid, lang="en"): 
    
    language = activate_language(lang)
    
    edit = True
    submission = get_object_or_404(Submission, uuid=uid)
    contact = submission.contact

    #check if one month has past from submission date, do not allow edit
    # http://stackoverflow.com/questions/4940583/subtract-dates-in-django
    today = datetime.datetime.today()
    today = today.replace(tzinfo=None)
    created = submission.date_created
    created = created.replace(tzinfo=None)
    
    dt = today - created
    days_diff = dt.days
    
    #msg = "today: " + str(today) + " ___ created: " + str(created) + "____ DIFF: " +  str(dt.days)
    
    if dt.days > 30:
        msg = "Editing submission is now closed. More than 30 days have passed since submission. %s days have passed. If you have any questions, please contact Linda Kollesh at lkollesh@sogc.com ." % days_diff
        return render(request, 'jb/message.html', { 'msg': msg, } )
    
    else:

        if request.method == "POST":
            positionform = PositionForm(request.POST, instance=submission)
            contactform = ContactForm(request.POST, instance=contact)

            if positionform.is_valid() and contactform.is_valid():
                contactform.save()
                this_submission = positionform.save(commit=False)
			    
                this_submission.date_modified = datetime.datetime.today()
                this_submission.save()
                
                return render(request, 'jb/thankyou.html', {'edit':edit, 'job': this_submission }) # send a variable to create custom message for edit
        
        else:
    		positionform = PositionForm(instance=submission)
    		contactform = ContactForm(instance=contact)

    return render_to_response("jb/editform.html", {'edit':edit, 'job':submission, 'positionform': positionform, 'contactform':contactform,  }, RequestContext(request))


def send_email(receiver, email_code, submission=None):
    
    the_email = Email.objects.get(code=str(email_code))
    the_content = the_email.body

    t = Template(the_content)
    c = Context({'contact': receiver, 'job': submission })

    html_content = t.render(c)
    text_content = strip_tags(html_content)

    subject, from_email = the_email.title, 'membership@sogc.com'
    to = receiver.email
    bcc_email = "lkollesh@sogc.com"
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to], bcc=[bcc_email])	

    msg.attach_alternative(html_content, "text/html")
    msg.send()