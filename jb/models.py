from django.db import models
from uuidfield import UUIDField
import datetime
#import itertools
#from django.utils.text import slugify
from encrypted_fields import EncryptedCharField
from import_export import resources, fields
from django.utils.translation import ugettext_lazy as _


LANGUAGES = (
	('en', 'English'),
	('fr', 'French')
)

CCTYPE = (
    ('visa', 'Visa'),
    ('mastercard', 'MasterCard')
)

EMPLOYMENT_TYPE = (
    ('Full-time', _('Full-time')),
    ('Part-time', _('Part-time'))
)

CONTRACT_TYPE = (
    ('Indeterminate/permanent', _('Indeterminate/permanent')),
    ('Term/contract', _('Term/contract')),
    ('Locum', _('Locum')),
)

POSITION_TYPE = (
    ('Academic', _('Academic')),
    ('Administrative', _('Administrative')),
    ('Obstetrics', _('Obstetrics')),
    ('Gynaecology', _('Gynaecology')),
    ('Obstetrics/Gynaecology', _('Obstetrics/Gynaecology')),
    ('Other', _('Other'))
)

RENEW_MONTHS = (
    (1, '1'),
    (2, '2'),
    (3, '3')
)


class Contact(models.Model):
    first_name = models.CharField(max_length=255, verbose_name=_('First Name'))
    last_name = models.CharField(max_length=255, verbose_name=_('Last Name'))
    email = models.CharField(max_length=254, verbose_name=_('Email'))
    phone = models.CharField(max_length=50, blank=True, verbose_name=_('Phone'))
    #ext = models.CharField(max_length=50, blank=True)
    address1 = models.CharField(max_length=255, verbose_name=_('Address 1'))
    address2 = models.CharField(max_length=255, blank=True, verbose_name=_('Address 2'))
    city = models.CharField(max_length=255, verbose_name=_('City'))
    province = models.CharField(max_length=255, verbose_name=_('Province / Territory / State'))
    country = models.CharField(max_length=255, verbose_name=_('Country'))
    postal_code = models.CharField(max_length=255, verbose_name=_('Postal Code'))
    company_name = models.CharField(max_length=255, blank=True, verbose_name=_('Company Name'))
    
    def __unicode__(self):
        string = unicode(self.first_name) + " " + unicode(self.last_name) 
        return string
	
class Payment(models.Model):
    posting_duration = models.IntegerField(null=True, choices=RENEW_MONTHS, verbose_name=_('Posting Duration (in months)'))
    ccType = models.CharField(max_length=50, choices=CCTYPE, verbose_name=_("Credit Card Type"))
    #ccNumber = fields.CreditCardField(max_length=255)
    ccNumber = EncryptedCharField(max_length=255, verbose_name=_("Credit Card Number"))
    ccHolderName = models.CharField(max_length=255, verbose_name=_("Name of Card Holder"))
    ccExpiry = models.DateField(verbose_name=_("Credit Card Expiry Date (mm/yy)"))
    ccSecurity = EncryptedCharField(max_length=50, verbose_name=_("Credit Card Security Code"))


class Submission(models.Model):
    uuid = UUIDField(auto=True)
    #position = models.OneToOneField(Position)
    contact = models.OneToOneField(Contact)
    payment = models.OneToOneField(Payment)
    show_contact = models.BooleanField(default=True, verbose_name=_("Show contact information in public posting"))
    language = models.CharField(max_length=50, choices=LANGUAGES, default='en')
    processed = models.BooleanField(default=False)
    #displayed = models.BooleanField(default=False)
    date_created = models.DateTimeField(auto_now=False, auto_now_add=True) #auto_now_add for creation
    #date_created = models.DateTimeField(default=datetime.datetime.now) #default to make editable
    date_modified = models.DateTimeField(auto_now=True, auto_now_add=False) #auto_now_add for creation
    date_to_remove = models.DateField(null=True, verbose_name='Date of Removal')
    #position fields
    title = models.CharField(max_length=255, verbose_name=_("Position Title"))
    company_name = models.CharField(max_length=255, verbose_name=_('Company Name'))
    division = models.CharField(max_length=255, blank=True, verbose_name=_('Division'))
    reference_num = models.CharField(max_length=255, blank=True, verbose_name=_("Reference Number"))
    url = models.CharField(max_length=255, blank=True, verbose_name=_("Job Posting URL"))
    city = models.CharField(max_length=255, verbose_name=_('City'))
    province = models.CharField(max_length=255, verbose_name=_('Province/ Territory / State'))
    country = models.CharField(max_length=255, verbose_name=_('Country'))
    salary_min = models.IntegerField(max_length=50, default=0, blank=True, verbose_name=_("Salary Minimum"))
    salary_max = models.IntegerField(max_length=50, default=0, blank=True, verbose_name=_("Salary Maximum"))
    salary_details = models.CharField(max_length=255, blank=True, verbose_name=_('Salary Details'))
    salary_negotiable = models.BooleanField(default=False, verbose_name=_('Salary Negotiable'))
    employment_type = models.CharField(max_length=255, blank=True, choices=EMPLOYMENT_TYPE, verbose_name=_("Type of Employment"))
    contract_type = models.CharField(max_length=255, blank=True, choices=CONTRACT_TYPE, verbose_name=_("Type of Contract"))
    position_type = models.CharField(max_length=255, blank=True, choices=POSITION_TYPE, verbose_name=_("Type of Position"))
    job_summary = models.TextField(blank=True, verbose_name=_("Job Summary"))
    roles_responsibilities = models.TextField(blank=True, verbose_name=_("Roles and Responsibilities"))
    skills_competencies = models.TextField(blank=True, verbose_name=_("Skills and Competencies"))
    education_experience = models.TextField(blank=True, verbose_name=_("Education and Experience"))
    other = models.TextField(blank=True, verbose_name=_('Other'))
    start_date = models.DateField(blank=True, null=True, verbose_name=_("Start Date of Employment (dd/mm/yyyy)")) #change to text field in case "Immediate"
    deadline_date = models.DateField(blank=True, null=True, verbose_name=_("Deadline Date of Application (dd/mm/yyyy)"))
    slug = models.SlugField(unique=True, blank=True)
    
    # checks if the removal date has passed and returns True if so
    @property
    def is_past_due(self):
        if datetime.date.today() is not None and self.date_to_remove is not None:
            if datetime.date.today() > self.date_to_remove:
                return True
            return False
        else:
            return False
    
    def __unicode__(self):
		return unicode(self.title)


class Email(models.Model):
	code = models.CharField(max_length=50, blank=False, unique=True)
	title = models.CharField(max_length=255)
	body = models.TextField()
	description = models.CharField(max_length=255, blank=True, null=True)

	def __unicode__(self):
		#return unicode(self.id)
		return unicode(self.title)
		
		
class SubmissionResource(resources.ModelResource):

    amount = fields.Field()
    invoicing = fields.Field()
    contact = fields.Field()
    description = fields.Field()
    attention_to = fields.Field()
    
    def dehydrate_amount(self, obj):
		months = obj.payment.posting_duration
		
		if months == 1:
		    return "$300 + $39.00 = $339.00"
		elif months == 2:
		    return "$500 + $65.00 = $565.00"
		elif months == 3:
		    return "$700 + $91.00 = $791.00"
		else:
		    return "--"
		    
    def dehydrate_invoicing(self, obj):
        return "Job posting"
        
    def dehydrate_description(self, obj):
        return "%s / %s" % (obj.title, obj.company_name)
        
    def dehydrate_contact(self, obj):
        return "%s %s %s %s, %s %s %s" % (obj.contact.company_name, obj.contact.address1, obj.contact.address2, obj.contact.city, obj.contact.province, obj.contact.postal_code, obj.contact.phone )
    
    def dehydrate_attention_to(self, obj):
        return "%s %s" % (obj.contact.first_name, obj.contact.last_name)
        
    class Meta:
        model = Submission
        fields = ('', 'date_created', )
		
	