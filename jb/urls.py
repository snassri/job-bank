from django.conf.urls import patterns, url
from jb import views

urlpatterns = patterns('',
        
    #url(r'^job-bank/$', views.jobs_list), 
    #url(r'^submission-form/$', views.submission_form), 
    url(r'^submission-form/(?P<lang>[a-zA-Z]{2})/$', views.submission_form), 
    url(r'^job-detail/(?P<lang>[a-zA-Z]{2})/(?P<post_slug>[^/]+)/$', views.job_detail, name="job_detail"), 
    url(r'^edit-submission/(?P<lang>[a-zA-Z]{2})/(?P<uid>\w+)/$', views.edit_submission, name='edit_submission'),
)