from django.contrib import admin
from jb.models import Submission, Contact, Payment, Email, SubmissionResource
import datetime
from dateutil.relativedelta import relativedelta
from django.conf.urls import patterns, url
from django.shortcuts import render, redirect, get_object_or_404
from django.http import HttpResponseRedirect, HttpResponse
from django.contrib import messages
from django.utils.html import strip_tags, format_html
from django.core import urlresolvers 
from django.db import models
from django.forms import TextInput, Textarea
from django.template import Context, Template
from django.core.mail import EmailMultiAlternatives
from import_export.admin import ImportExportModelAdmin
from django.db.models.signals import post_delete
from django.dispatch import receiver

class SubmissionAdmin(ImportExportModelAdmin):
	
    resource_class = SubmissionResource
    pass
	
    list_per_page = 1000
    list_display = ('edit_link', 'company_name', 'position_title', 'contact', 'date_created', 'date_to_remove', 'processed' )
    search_fields = ['contact__first_name', 'contact__last_name', 'company_name', 'title', ]
    
    fieldsets = ( 
		('Submission', { 'fields': ('uuid', 'date_created', 'date_modified', 'language', 'processed', 'date_to_remove', 'renew_buttons', 'posting_duration', )}),
		('Position', { 'fields': ('title', 'company_name', 'division', 'reference_num', 'url', 'city', 'province', 'country', 'salary_min', 'salary_max', 'salary_details', 'salary_negotiable', 'employment_type', 'contract_type', 'position_type', 'job_summary', 'roles_responsibilities', 'skills_competencies', 'education_experience', 'other', 'start_date', 'deadline_date', )}),
		('Contact', { 'fields': ('show_contact', 'contact_fullname', 'contact_email', 'contact_phone', 'contact_address1', 'contact_address2', 'contact_city', 'contact_province', 'contact_country', 'contact_postalcode', )}),
		('Payment', { 'fields': ('cost', 'cc_type', 'cc_number', 'cc_name', 'cc_expiry', 'cc_security', )}),
	)

    readonly_fields = ('uuid', 'date_created', 'date_modified', 'language', 'processed', 'renew_buttons', 'posting_duration', 'contact_province', 'contact_city', 'contact_country', 'contact_fullname', 'contact_email', 'contact_address2', 'contact_address1', 'contact_postalcode', 'contact_phone', 'cost', 'cc_type', 'cc_number', 'cc_name', 'cc_expiry', 'cc_security', )
    actions = ['mark_processed', 'unprocess',  ]
		    
    
    def renew_buttons(self, instance):
        return format_html(u'<a href="renew-posting/1">1 month</a> or <a href="renew-posting/2">2 months</a> or <a href="renew-posting/3">3 months</a>')
    
    renew_buttons.short_description = "Renew post (from today's date)"
    
    #def renew_buttons(self, instance):
		#change_url = urlresolvers.reverse(self.admin_site.admin_views, kwargs={'uid': instance.id, 'months':'1' })
		#return 'Hello: <a href="%s"> %s </a>' % (change_url, change_url)
	
	
    def get_urls(self):
        urls = super(SubmissionAdmin, self).get_urls()
        my_urls = patterns('',
            (r'^my_view/$', self.admin_site.admin_view(self.my_view)),
	        url(r'(?P<submission_id>\w+)/process-submission/$', self.admin_site.admin_view(self.process_submission), name='process_submission' ),
	        url(r'(?P<submission_id>\w+)/remove-submission/$', self.admin_site.admin_view(self.remove_submission), name='remove_submission' ),
	        url(r'(?P<submission_id>\w+)/renew-posting/(?P<months>\w+)/$', self.renew_posting, name='renew_posting' ),
	    )
        return my_urls + urls

        
    def my_view(self, request):
        return HttpResponse("Hello!")
        
    def cost(self, obj):
        if obj.payment.posting_duration == 1:
            return "$339.00"
        elif obj.payment.posting_duration == 2:
            return "$565.00"
        elif obj.payment.posting_duration == 3:
            return "$791.00"
        else:
            return "--"
        
		
    def renew_posting(self, request, submission_id, months):
        
        the_submission = Submission.objects.get(pk=submission_id)

        num_months = 0
        
        if months is not None:
            if months == "1":
                num_months = 1
            elif months == "2":
                num_months = 2
            elif months == "3":
                num_months = 3
            
        the_submission.payment.posting_duration = num_months
        the_submission.payment.save(update_fields=['posting_duration'])    
        
        
        # was adding months to the removal date previously
        '''
        if the_submission.date_to_remove == None:
            the_submission.date_to_remove = datetime.date.today()
        
        curr_date = the_submission.date_to_remove
        '''
        
        # now adding months to today's date
        curr_date = datetime.date.today()
        new_date = curr_date + relativedelta(months=num_months)
        the_submission.date_to_remove = new_date
        
        the_submission.save(update_fields=['date_to_remove'])
        
        return redirect('admin:jb_submission_change', the_submission.id)
	  
    
    def process_submission(self, request, submission_id):
		
        the_submission = Submission.objects.get(pk=submission_id)
        the_submission.processed = True
        
        #set the date of removal based on duration
        if the_submission.payment.posting_duration is not None:
            removal_date = datetime.date.today() + relativedelta(months=the_submission.payment.posting_duration)
            the_submission.date_to_remove = removal_date
        
        the_submission.save(update_fields=['processed', 'date_to_remove'])
        self.message_user(request, "Submission processed and confirmation email sent", messages.INFO)
        
        if the_submission.language == "fr":
            send_email("processed-inform-FR", the_submission)
        else:
            send_email("processed-inform", the_submission)
            
        
        return redirect('admin:jb_submission_change', the_submission.id )
        #return redirect('admin:jb_submission_changelist')
	        
		
		
    def remove_submission(self, request, submission_id):

		the_submission = Submission.objects.get(pk=submission_id)
		the_submission.processed = False
		the_submission.save(update_fields=['processed'])
		
		self.message_user(request, 'Submission marked as uproccessed and is removed from job bank list')
		return redirect('admin:jb_submission_change', the_submission.id)

    
    # this function marks the job posting as processed and creates date of removal based on posting_duration
    def mark_processed(self, request, queryset):
        queryset.update(processed=True)
        
        for job in queryset:
            if job.payment.posting_duration is not None:
                removal_date = datetime.date.today() + relativedelta(months=job.payment.posting_duration)
                job.date_to_remove = removal_date
                job.save(update_fields=['date_to_remove'])
        
            #if job.language == "fr":
               # send_email("processed-inform-FR", job)
            #else:
                #send_email("processed-inform", job)
                
        self.message_user(request, 'Submission(s) marked as processed and confirmation emails sent')
        
    mark_processed.short_description = "Mark as processed"
    
    
    def unprocess(self, request, queryset):
        queryset.update(processed=False)
        self.message_user(request, 'Submission(s) unprocessed')
        
    unprocess.short_description = "Mark as NOT processed"
         
    
    def edit_link(self, obj):
        return "edit"
    edit_link.short_description = "edit"
    
    
     ####  position fields  ####
   
    def position_title(self, obj):
		change_url = urlresolvers.reverse('jb:job_detail', kwargs={'post_slug': obj.slug, 'lang': "en"})
		return '<a href="%s">%s</a>' % (change_url, obj.title)

	#position_title.short_description = 'Position title'
    position_title.allow_tags = True
    position_title.admin_order_field = 'title'
    
    ####  contact fields  ####
    
    def contact_fullname(self, obj):
        fullname = unicode(obj.contact.first_name) + " " + unicode(obj.contact.last_name)
        return fullname
    contact_fullname.short_description = "Full Name"
        
    def contact_email(self, obj):
        return obj.contact.email
    contact_email.short_description = "Email"
    
    def contact_phone(self, obj):
        return obj.contact.phone
    contact_phone.short_description = "Phone"
        
    def contact_address1(self, obj):
        return obj.contact.address1
    contact_address1.short_description = "Address 1"
    
    def contact_address2(self, obj):
        return obj.contact.address2
    contact_address2.short_description = "Address 2"
        
    def contact_city(self, obj):
        return obj.contact.city
    contact_city.short_description = "City"
    
    def contact_province(self, obj):
        return obj.contact.province
    contact_province.short_description = "Province"
    
    def contact_country(self, obj):
        return obj.contact.country
    contact_country.short_description = "Country"
    
    def contact_postalcode(self, obj):
        return obj.contact.postal_code
    contact_postalcode.short_description = "Postal Code"
        
    ####  payment fields  ####
    
    def posting_duration(self, obj):
        return obj.payment.posting_duration
        
    def cc_type(self, obj):
        return obj.payment.ccType
    cc_type.short_description = "Credit Card Type"
        
    def cc_number(self, obj):
        return obj.payment.ccNumber
    cc_number.short_description = "Credit Card Number"
        
    def cc_name(self, obj):
        return obj.payment.ccHolderName
    cc_name.short_description = "Credit Card Holder Name"
    
    def cc_expiry(self, obj):
        return obj.payment.ccExpiry.strftime("%m/%y")
    cc_expiry.short_description = "Credit Card Expiry Date (mm/yy)"
        
    def cc_security(self, obj):
        return obj.payment.ccSecurity
    cc_security.short_description = "Credit Card Security Code"
 
 
@receiver(post_delete, sender=Submission)
def post_delete_foreignkeys(sender, instance, *args, **kwargs):
    if instance.contact: # just in case contact is not specified
        instance.contact.delete()       
    if instance.payment:
        instance.payment.delete()



class EmailAdmin(admin.ModelAdmin):
	
	formfield_overrides = {
		models.CharField: {'widget': TextInput(attrs={'size':142, })},
		models.TextField: {'widget': Textarea(attrs={'rows':30, 'cols':140})},
	}


	fields = ['code', 'title', 'body', 'description', 'send_test_btn',  ]
	readonly_fields = ('send_test_btn',)

	list_display = ('edit_link', '__unicode__', 'code', 'description', )

	def edit_link(self, obj):
		return "edit"
	edit_link.short_description = "edit"


	def get_urls(self):
		urls = super(EmailAdmin, self).get_urls()
		my_urls = patterns('',
			url(r'(?P<email_id>\w+)/email-test/$', self.admin_site.admin_view(self.email_test) ),
		)
		return my_urls + urls

	
	def send_test_btn(self, instance):
		return format_html(u'<a href="email-test/">Send Test</a>')
	
	
	def email_test(self, request, email_id):
	    the_email = Email.objects.get(pk=email_id)
	    the_content = the_email.body
	    random_submission = Submission.objects.order_by('?')[0]
	    contact = random_submission.contact
	    
	    t = Template(the_content)
	    c = Context({ 'contact':contact, 'job':random_submission, })
	    html_content = t.render(c)
	    text_content = strip_tags(html_content)
	    subject, from_email = '[Test] '+ the_email.title, 'memberhsip@sogc.com'
	    to = "lkollesh@sogc.com"
	    msg = EmailMultiAlternatives(subject, text_content, from_email, [to])
	    msg.attach_alternative(html_content, "text/html")
	    msg.send()
	    
	    self.message_user(request, 'Email sent successfully')
	    return redirect('admin:am_email_change', the_email.id )
		
		

def send_email(email_code, submission):

    the_email = Email.objects.get(code=str(email_code))
    the_content = the_email.body
	
    contact = submission.contact

    t = Template(the_content)
    c = Context({'contact': contact, 'job':submission })

    html_content = t.render(c)
    text_content = strip_tags(html_content)

    subject, from_email = the_email.title, 'membership@sogc.com' #change from_email to keyword for easier change
    
    to = contact.email #uncomment this for production
    bcc_email = "lkollesh@sogc.com"
    msg = EmailMultiAlternatives(subject, text_content, from_email, [to], bcc=[bcc_email], )
	
    msg.attach_alternative(html_content, "text/html")
    msg.send()
	
    
admin.site.register(Submission, SubmissionAdmin)
admin.site.register(Email, EmailAdmin)
#admin.site.register(Contact)

    