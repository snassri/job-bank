# -*- coding: utf-8 -*-
from django import forms
from django.forms.util import ErrorList
from django.forms.forms import NON_FIELD_ERRORS
from jb.models import Submission, Contact, Payment
from django.utils.translation import ugettext_lazy as _


class PositionForm(forms.ModelForm):
	
	start_date = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), input_formats=['%d/%m/%Y'], error_messages={'invalid': _('Please enter a date in the format dd/mm/yyyy')}, required=False)
	deadline_date = forms.DateField(widget=forms.DateInput(format='%d/%m/%Y'), input_formats=['%d/%m/%Y'], error_messages={'invalid': _('Please enter a date in the format dd/mm/yyyy')}, required=False)
	
	class Meta:
		model = Submission
		exclude = ['uuid', 'contact', 'payment', 'language', 'processed', 'displayed', 'date_created', 'date_modified', 'date_to_remove', 'slug']
		#fields = '__all__'
		
	def __init__(self, *args, **kwargs):
		super(PositionForm, self).__init__(*args, **kwargs)
		self.fields['start_date'].label = _("Start Date of Employment (dd/mm/yyyy)")
		self.fields['deadline_date'].label = _("Deadline Date of Application (dd/mm/yyyy)")
		
	
class ContactForm(forms.ModelForm):
    
    class Meta:
        model = Contact
        fields = '__all__'
    
   
class PaymentForm(forms.ModelForm):
	
	ccExpiry = forms.DateField(widget=forms.DateInput(format='%m/%y'), input_formats=['%m/%y'], error_messages={'invalid': _('Please enter a date in the format mm/yy')}, required=True)
	
	class Meta:
		model = Payment
		fields = '__all__'
		
	def __init__(self, *args, **kwargs):
		super(PaymentForm, self).__init__(*args, **kwargs)
		self.fields['ccExpiry'].label = _("Credit Card Expiry Date (mm/yy)")


	
        
        
